package com.edu.Fitnesspower.services;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.edu.Fitnesspower.entities.Role;
import com.edu.Fitnesspower.entities.Utilisateur;
import com.edu.Fitnesspower.repostories.UtilisateurRespository;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {

	@Autowired
	private UtilisateurRespository utilisateurRespository;
	
	//cryptage de password
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public List<Utilisateur> getAllUtilisateurs() {

		return utilisateurRespository.findAll();
	}

	@Override
	public Utilisateur findById(int id) {
		Optional<Utilisateur> utOptional = utilisateurRespository.findById(id);

		if (utOptional.isEmpty()) {
			return null;
		} else {
			return utOptional.get();
		}

	}

	@Override
	public Utilisateur createUtlisateur(Utilisateur utilisateur) {
		String cryptedPassword = bCryptPasswordEncoder.encode(utilisateur.getPassword());
		utilisateur.setPassword(cryptedPassword);
		return utilisateurRespository.save(utilisateur);
	}

	@Override
	public Utilisateur updateUtlisateur(Utilisateur utilisateur) {
		Optional<Utilisateur> utOptional = utilisateurRespository.findById(utilisateur.getId());

		if (utOptional.isEmpty()) {
			return null;
		} else {
			String cryptedPassword = bCryptPasswordEncoder.encode(utilisateur.getPassword());
			utilisateur.setPassword(cryptedPassword);
			return utilisateurRespository.save(utilisateur);
		}
	}

	@Override
	public void deleteUtlisateur(int id) {
		utilisateurRespository.deleteById(id);

	}

	@Override
	public List<Utilisateur> findByNom(String nom) {
		
		return utilisateurRespository.findByNom(nom);
	}

	@Override
	public List<Utilisateur> findByNomAndPrenom(String nom, String prenom) {
		
		return utilisateurRespository.findByNomAndPrenomWithJPQL(nom, prenom);
	}

	@Override
	public List<Utilisateur> findByAgeIn(List<Integer> ages) {
		
		return utilisateurRespository.findByAgeIn(ages);
	}



	@Override
	public List<Utilisateur> findByRolesTitre(String titre) {
		return utilisateurRespository.findByRolesTitre(titre);
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Utilisateur utilisateur = utilisateurRespository.findByEmail(email);
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(Role r: utilisateur.getRoles()) {
			GrantedAuthority authority= new SimpleGrantedAuthority(r.getTitre());
			authorities.add(authority);
		}
		
		return new User(utilisateur.getEmail(),utilisateur.getPassword(),authorities);
	}

}
