package com.edu.Fitnesspower.repostories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.edu.Fitnesspower.entities.Utilisateur;

@Repository
public interface UtilisateurRespository extends JpaRepository<Utilisateur, Integer> {
	
	public List<Utilisateur> findByNom(String nom);
	public Utilisateur findByEmail(String email);
	
	
	public List<Utilisateur> findByNomAndPrenom(String nom, String prenom);
	
	
	@Query("SELECT u FROM Utilisateur u WHERE u.nom = ?1 OR u.prenom = ?2 ")
	public List<Utilisateur> findByNomAndPrenomWithJPQL(String nom, String prenom);
	
	@Query("SELECT u FROM Utilisateur u WHERE u.nom LIKE :myNom OR u.prenom LIKE :myPrenom")
	public List<Utilisateur> findByNomAndPrenomWithJPQLWithNamedParameters(@Param(value = "myNom") String nom,@Param(value = "myPrenom") String prenom);
	
	public List<Utilisateur> findByAgeIn(List<Integer> ages);
	
	public List<Utilisateur> findByRolesTitre(String titre);


}
