package com.edu.Fitnesspower.controllers;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edu.Fitnesspower.entities.Utilisateur;
import com.edu.Fitnesspower.requests.NomAndPrenomRequest;
import com.edu.Fitnesspower.services.UtilisateurService;

@RestController
@RequestMapping("/utilisateur") // localhost:8080/utilisateur
public class UtilisateurController {

	@Autowired
	private UtilisateurService utilisateurService;

	@GetMapping
	public List<Utilisateur> getAllUtilisateurs() {
		return utilisateurService.getAllUtilisateurs();
	}
	// localhost:8080/utilisateur/2
	@GetMapping(path = "/{id}") 
	public ResponseEntity<Utilisateur> findUtilisateurById(@PathVariable int id) {
		Utilisateur utilisateur = utilisateurService.findById(id);

		if (utilisateur == null) {
			return new ResponseEntity<Utilisateur>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Utilisateur>(utilisateur, HttpStatus.OK);
		}
	}
	// localhost:8080/utilisateur/findByFirstName/oumaima
	@GetMapping(path = "/findByFirstName/{firstName}") 
	public ResponseEntity<List<Utilisateur>> findUtilisateurBynom(@PathVariable String nom) {
		List<Utilisateur> utilisateurs = utilisateurService.findByNom(nom);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}

	// localhost:8080/utilisateur/findByNomAndPrenom/oumaima/guedri
	@GetMapping(path = "/findByNomAndPrenom/{nom}/{prenom}")
	public ResponseEntity<List<Utilisateur>> findByNomAndPrenom(@PathVariable String nom,
			@PathVariable String prenom) {
		List<Utilisateur> utilisateurs = utilisateurService.findByNomAndPrenom(nom, prenom);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}

	// localhost:8080/utilisateur/findByNomAndPrenomWithRB
	@GetMapping(path = "/findByNomAndPrenomWithRB")
	public ResponseEntity<List<Utilisateur>> findUtilisateurByNomAndPrenom(
			@RequestBody NomAndPrenomRequest nomAndPrenomRequest) {
		List<Utilisateur> utilisateurs = utilisateurService.findByNomAndPrenom(
		nomAndPrenomRequest.getNom(), nomAndPrenomRequest.getPrenom());
		
		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}

	// localhost:8080/utilisateur/findByFirstNameAndLastNameWithRB
	@GetMapping(path = "/findByAge")
	public ResponseEntity<List<Utilisateur>> findUtilisateurByAge(@RequestBody List<Integer> ages) {
		List<Utilisateur> utilisateurs = utilisateurService.findByAgeIn(ages);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}


	@GetMapping(path = "/findByRoleTitre/{titre}") // localhost:8080/utilisateur/findByRoleTitre/ADMIN
	public ResponseEntity<List<Utilisateur>> findUtilisateurByRole(@PathVariable String titre) {
		List<Utilisateur> utilisateurs = utilisateurService.findByRolesTitre(titre);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}


	@PostMapping
	public Utilisateur createUtilisateur(@RequestBody Utilisateur utilisateur) {
		return utilisateurService.createUtlisateur(utilisateur);
	}

	@PutMapping
	public Utilisateur updateUtilisateur(@RequestBody Utilisateur utilisateur) {
		return utilisateurService.updateUtlisateur(utilisateur);
	}
	// localhost:8080/utilisateur/3
	@DeleteMapping(path = "/{id}") 
	public void deleteUtilisateur(@PathVariable int id) {
		utilisateurService.deleteUtlisateur(id);
	}

}
