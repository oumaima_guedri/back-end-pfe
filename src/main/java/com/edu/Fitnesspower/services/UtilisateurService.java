package com.edu.Fitnesspower.services;

import java.sql.Date;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.edu.Fitnesspower.entities.Utilisateur;

public interface UtilisateurService extends UserDetailsService {
	
	// methodes CRUD Basiques
	public List<Utilisateur> getAllUtilisateurs();
	public Utilisateur findById(int id);
	public Utilisateur createUtlisateur(Utilisateur utilisateur);
	public Utilisateur updateUtlisateur(Utilisateur utilisateur);
	public void deleteUtlisateur(int id);
	
	
	// methodes avancees
	public List<Utilisateur> findByNom(String nom);
	public List<Utilisateur> findByNomAndPrenom(String nom, String prenom);
	public List<Utilisateur> findByAgeIn(List<Integer> ages);
	public List<Utilisateur> findByRolesTitre(String titre);
	
	
	

}
