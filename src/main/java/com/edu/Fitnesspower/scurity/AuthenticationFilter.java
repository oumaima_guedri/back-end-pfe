package com.edu.Fitnesspower.scurity;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.edu.Fitnesspower.requests.UtilisateurLoginRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter{
	
	private AuthenticationManager authenticationManager;
	
	public AuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager=authenticationManager;
	}
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		
		
		try {
			UtilisateurLoginRequest loginRequest= new ObjectMapper().readValue(request.getInputStream(),UtilisateurLoginRequest.class);
			
			
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword());
			return authenticationManager.authenticate(authentication);
		} catch (IOException e) {
		
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		User user = (User) authResult.getPrincipal();  
		
		String token= Jwts.builder().setSubject(user.getUsername())
				.setExpiration(new Date(System.currentTimeMillis()+999999999))
				.signWith(io.jsonwebtoken.SignatureAlgorithm.HS512, SecurityConstants.SECRET_JWT)
				.compact();
		
		response.addHeader(SecurityConstants.AUTORIZATION, SecurityConstants.BEARER+token);
		
	}

}
